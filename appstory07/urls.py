from django.urls import path
from . import views

app_name = 'appstory07'

urlpatterns = [
    path('', views.index, name='index'),
	path('story10/', views.loadbalancer, name='loadbalancer'),
]