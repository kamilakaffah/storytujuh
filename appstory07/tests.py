from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
#from django.utils import timezone
from django.http import HttpRequest
import unittest
#from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.chrome.options import Options
from .views import *
#from .models import ModelStatus
#from .forms import Status
#from datetime import date
#import time

# Create your tests here.
class UrlTest(TestCase):
    def test_url_valid(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_invalid(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

class ApplicationTest(TestCase):
    def test_index_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_index_contains_some_text(self):
        found = resolve('/')
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, Here's My Profile", response_content)
